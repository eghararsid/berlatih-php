<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Belajar String PHP</h1>
    
	<?php
		$hello ="Hello World!";
		// echo
		echo $hello . "<br>";
		print $hello . "<br>";
		print_r($hello);
		echo "<br>";
		var_dump($hello) . "<br>";
		
		echo "<br> Panjang kata: " . strlen($hello) . "<br>";
		
		echo strpos($hello, "ell") . "<br>";
		
		echo "Jumlah kata: " . print_r(explode(' ', $hello)) . "<br>";
		echo "Jumlah kata: " . str_word_count($hello) . "<br>";
		echo str_replace("Hello ", "Saya.", $hello) . "<br>";
		
		if (true && false) {
			echo "Salah<br>";
		} else {
			echo "Benar<br>";
		}
		
		$siswa = array("regi", "bobby", "ahmad");
		print_r($siswa);
		echo "<br>" . $siswa[1] . "<br>";
		
		$siswa[] = "Farel";
		print_r($siswa);
		echo "<br>";
		
		array_push($siswa, "Putra", "Putri");
		print_r($siswa);
		echo "<br>";
		echo count($siswa) . "<br>";
		
		$siswa1 = [
			"Name" => "Will Byers",
            "Age" => 12,
            "nilai" => 70
        ];
		print_r($siswa1);
		echo "<br>";

		// menambahkan key value baru ke array $siswa1
		// $siswa1["email"] = "abduh@mail.com";
		// $siswa1["nama"] = "Farel";

		print_r($siswa1);
		echo "<br>";
		
		$trainer = array (
			array(
				"Name" => "Rezky",
				"Course" => "Laravel"
			),
			array(
				"Name" => "Abduh",
				"Course" => "Adonis"
			),
			array(
				"Name" => "Iqbal",
				"Course" => "VueJs"
			),
		);

		echo $trainer[0]["Name"] . "<br>";
		
		echo "<pre>";
		print_r($trainer);
		echo "</pre>";
	?>
</body>
</html>